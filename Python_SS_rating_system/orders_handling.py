#This class will rate all the popular items with their accompanied list

from collections import OrderedDict

class OrderSummary:

    @classmethod
    def __normalize_articles_in_an_order(self,the_category_popular_articles,popular_item_accompanied_orders):                                    
        for a_popular_article in popular_item_accompanied_orders:
            d=OrderedDict()                     
            an_order_accompanied_articles=a_popular_article.accompanied_articles             
            # Processing for different situaltion
            for a_single_acc_article_name in an_order_accompanied_articles:
                a_single_acc_article=an_order_accompanied_articles[a_single_acc_article_name]
                
                if a_single_acc_article['units'] < a_popular_article.units:
                    d[a_single_acc_article_name]=a_single_acc_article['units']
                else:
                    d[a_single_acc_article_name]=a_popular_article.units
                # Conditioner 1 [If the accompanied article is already in popular list we give less weight to it]
                if a_single_acc_article_name in the_category_popular_articles:
                    d[a_single_acc_article_name] *= 0.9                
                # Conditioner 2 [if the accompanied article has same main and/or ordinary category]
                if a_single_acc_article['category'] == a_popular_article.category and a_single_acc_article['main_category'] == a_popular_article.main_category:
                    d[a_single_acc_article_name] *= 0.75
                elif a_single_acc_article['category'] == a_popular_article.category:
                    d[a_single_acc_article_name] *= 0.85
                elif a_single_acc_article['main_category'] == a_popular_article.main_category:
                    d[a_single_acc_article_name] *= 0.95
                # Conditioner 3-Last Normalize all the ranks fall between (0,1]
                d[a_single_acc_article_name] /= a_popular_article.units

            a_popular_article.accompanied_articles_rates_normalized=d

    @classmethod
    def __merge_nomalized_articles_in_all_orders_for_cashier_group(self,populated_accompanied_articles): 
        articles_ref=OrderedDict()
        articles_rev_ref=OrderedDict()
        
        
        for a_cashier_group_no in populated_accompanied_articles:                             
            pa=OrderedDict()
            group_articles=populated_accompanied_articles[a_cashier_group_no] # Later on d should be added to this and this must be added to m
            group_accompanied_articles=group_articles['accompanied_products']
            for popular_artcile_of_the_group_name,order_base_accart in group_accompanied_articles.items():
                d=OrderedDict()               
                
                print(" Merging for ["+popular_artcile_of_the_group_name+"]:")

                for an_order in order_base_accart:             
                    for ac in tuple(an_order.accompanied_articles_rates_normalized):
                        if ac in d:
                            d[ac] +=an_order.accompanied_articles_rates_normalized[ac]
                        else:
                            d[ac] =an_order.accompanied_articles_rates_normalized[ac]
                            if not ac in articles_ref:
                                articles_ref[ac]={'article_id':an_order.accompanied_articles[ac]['article_id'],\
                                                  'category':an_order.accompanied_articles[ac]['category'],\
                                                  'main_category':an_order.accompanied_articles[ac]['main_category'],\
                                                  'is_popular':0,\
                                                  'in_groups':[a_cashier_group_no]}
                                articles_rev_ref[an_order.accompanied_articles[ac]['article_id']]=ac


                # Adding pop article if needed
                if popular_artcile_of_the_group_name in articles_ref:
                    articles_ref[popular_artcile_of_the_group_name]['is_popular']=1
                else:
                    articles_ref[popular_artcile_of_the_group_name]={'article_id':an_order.article_id,\
                                              'category':an_order.category,\
                                              'main_category':an_order.main_category,\
                                              'is_popular':1}
                    articles_rev_ref[an_order.article_id]=popular_artcile_of_the_group_name

                l=sorted(d.items(), key=lambda x:x[1], reverse = True)
                # d should get scaled in the range of [0,100]                
                max_rate=l[0][1]   # Has highest value
                d=OrderedDict({k:(round((v/max_rate)*100.0,3)) for k,v in d.items()})
                d=OrderedDict(sorted(d.items(), key=lambda x:x[1], reverse = True))
                pa[popular_artcile_of_the_group_name]=d
                for m_art in pa[popular_artcile_of_the_group_name]:
                    print("\t\t|--"+" (nv:{0:7.3})".format(d[m_art])+" ---------> "+m_art)
                print("-"*25+" {0} items merged".format(len(d))+"-"*25)
            
            populated_accompanied_articles[a_cashier_group_no]['merged_normalized_rates']=pa
        return (articles_ref,articles_rev_ref)

    @classmethod
    def __normalize_base_on_orders(self,populated_accompanied_articles):
        for cashier_group_no in populated_accompanied_articles:
            group_popular_items=populated_accompanied_articles[cashier_group_no]['accompanied_products']
            for a_popular_item in group_popular_items:
                OrderSummary.__normalize_articles_in_an_order(populated_accompanied_articles[cashier_group_no]['category_popular_articles'],group_popular_items[a_popular_item])    
            

    def __init__(self,populated_accompanied_articles):
        OrderSummary.__normalize_base_on_orders(populated_accompanied_articles)
        #after normalizing the items accompanied articles should get merged
        
        self.ArticlesRef,articles_ref_rev=OrderSummary.__merge_nomalized_articles_in_all_orders_for_cashier_group(populated_accompanied_articles)
        self.ArticlesRefRev=OrderedDict(sorted(articles_ref_rev.items(), key=lambda x:x[1],reverse=False))
        self.SummarizedandProcessedArticles=populated_accompanied_articles
