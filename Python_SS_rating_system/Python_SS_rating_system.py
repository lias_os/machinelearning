'''
 This python application will take care of rating 
 the bought Items in the multi item orders 
'''
from connect_to_db import *
from item_processing import *
from helper_data_processors import *
from orders_handling import *
from processed_data_dumper import *
import dill,time



print('Started...')
start_time=time.time()
#db=Database('mysql+cymysql://root:root@127.0.0.1:3306/globen?charset=utf8')
#db=Database('mysql+cymysql://root:root@127.0.0.1:3306/test_laxbutikenheberg?charset=utf8')
#op=OrdersProcessing(4500,1)

#result = db.execute_sql(op.nlatest_multiatricle_orders_sql)
#records = db.return_query_records(result)

#with open("sql_records_lax.dill", "wb") as dill_file:
#    dill.dump(records,dill_file)
#    dill_file.close()

with open("sql_records_lax.dill", "rb") as dill_file:
    records=dill.load(dill_file)
    dill_file.close()


#cshp=CashierProcessing()
#result=db.execute_sql(cshp.query_all_cashiers_categories_sql)
#cashiers_records=db.return_query_records(result)
#cashier_groups=CashierProcessing.extract_cashier_categories(cashiers_records)

#cp=CategoryProcessing()
#categories_result=db.execute_sql(cp.query_all_categories_sql)
#categories_records=db.return_query_records(categories_result)
#cp.extract_categories(categories_records)    
#listed_cashier_ids=OrdersProcessing.extract_all_cashiers_ids(records)
#useful_cashier_groups=CashierProcessing.flag_listed_cashiers_in_records(listed_cashier_ids,cashier_groups)

#with open("sql_useful_cashier_groups_lax.dill", "wb") as dill_file:
#    dill.dump(useful_cashier_groups,dill_file)
#    dill_file.close()

with open("sql_useful_cashier_groups_lax.dill", "rb") as dill_file:
    useful_cashier_groups=dill.load(dill_file)
    dill_file.close()

all_active_cashiers_popular_articles=OrdersProcessing.create_list_of_cashier_groups_most_sold_items(records,useful_cashier_groups,8)
paap=OrdersProcessing.populate_accompanied_articles_for_popular_items_in_cashregisters(records,all_active_cashiers_popular_articles)

#with open("paap.dill", "wb") as dill_file:
#    dill.dump(paap,dill_file)
#    dill_file.close()

with open("paap.dill", "rb") as dill_file:
    paap=dill.load(dill_file)
#    dill_file.close()


# Populating accompanied articles for each of the popular articles for 
order_summary=OrderSummary(paap)
dd=DataDumper(order_summary)
generated_db_filenames,generated_group_articles_info_filenames,generated_db_filenames_json=dd.SaveRatingsIDBased([],include_categories=False,articles_as_users=False)
dd.SaveArticlesInfo()
dd.SaveArticlesInRatingsFileInfo(generated_db_filenames,generated_group_articles_info_filenames)
dd.SaveGroupOrdersAsJsonFile(generated_db_filenames,generated_db_filenames_json)

print('Finished')
elapsed_time=time.time()-start_time
eltm_str=time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
print('Elapsed time :',eltm_str)
#for r in records:
#    print(r)

