
'''
Here we have helper methods that are supposed to   
'''

from collections import OrderedDict
class ArticleInOrder:
    def __get_orderid_items(self,records,the_orderid):  #This will retrun all the items inside an orderid claen and neat with repetition as dict
        order_articles=list(filter(lambda x:x.orderid == the_orderid,records))
        result=OrderedDict()
        for item in order_articles:            
            result[item.artikelnamn] = {'id':item.id,'cashier_id':item.kassaid,'article_id':item.artikelid,'units':item.antal,'category':item.kategori,'main_category':item.huvudkategori}
        return {'timestamp':item.datum,'result':result}

    def __compute_accompanied_list_normalized_rates_my_method(self):
        # This is Sina's method of rating an article between (0,1)
        #for ac in self.accompanied_articles                       
        pass
       
    
    def __init__(self,records,article_name,orderid):
        self.name=article_name        
        self.order_id=orderid
        goi=self.__get_orderid_items(records,orderid)# Should contain name and units of the accompanied articles
        self.timestamp=goi['timestamp']
        self.accompanied_articles=goi['result']
        # The order must have the article_name inside it

        self.the_article_tuple=self.accompanied_articles[article_name]
        self.article_id=self.accompanied_articles[article_name]['article_id']
        self.units=self.accompanied_articles[article_name]['units']        
        self.category=self.accompanied_articles[article_name]['category']
        self.main_category=self.accompanied_articles[article_name]['main_category']
        self.id=self.accompanied_articles[article_name]['id']
        self.cashier_id=self.accompanied_articles[article_name]['cashier_id']

        #self.articleid
        del self.accompanied_articles[article_name]
        self.accompanied_articles_rates_normalized=self.__compute_accompanied_list_normalized_rates_my_method()    
class OrdersProcessing:
    def __sql_cmd_nlatest_multiatricle_orders(self,limit,minimum_different_articles_in_receipt):
       #cmd_str= '''SELECT kb.id,kb.artikelid,kb.artikelnamn,kb.antal,kb.kassaid,kb.datum,kb.orderid,kb.fel FROM klara_bestallningar kb LIMIT 100'''
        #"""
        cmd_str='''SELECT kb.id,kb.artikelid,kb.artikelnamn,kb.antal,kb.kategori,kb.huvudkategori,kb.kassaid,kb.datum,kb.orderid,kb.fel FROM klara_bestallningar kb
                    INNER JOIN (SELECT orderid,fel
				        FROM klara_bestallningar					
				        GROUP BY orderid
				        HAVING COUNT(orderid) > {0:d}) dup				
				            ON kb.orderid = dup.orderid AND kb.fel = 0 AND kb.antal > 0
				            ORDER BY orderid DESC								
				            LIMIT {1:d};
                '''.format(minimum_different_articles_in_receipt,limit)  
#"""   
        return cmd_str
    @classmethod
    def __get_most_popular_articles_names(self,records,cashiers_list,number_of_top_articles):
        d=OrderedDict()
        for r in records:
            currecnt_article_str=r.artikelnamn
            currecnt_article_units=r.antal
            current_article_cashier=r.kassaid
            for cashier_id in cashiers_list:
                if current_article_cashier == cashier_id:  
                    if currecnt_article_str in d:                
                        d[currecnt_article_str] += currecnt_article_units
                    else:
                        d[currecnt_article_str] = currecnt_article_units                

            most_popular_articles = OrderedDict(sorted(d.items(), key=lambda x:x[1],reverse = True) [0:number_of_top_articles])     
        return most_popular_articles
    
    @classmethod
    def create_accompanied_products_tuples_by_names(self,records,cashiers_list,popular_articles):
        popular_articles_accompanied_list=OrderedDict()        
        for p in popular_articles:
            popular_articles_accompanied_list[p]=[]
        for an_article in records:                        
            if an_article.kassaid in cashiers_list:





                if an_article.artikelnamn in popular_articles:                   
                    aio=ArticleInOrder(records,an_article.artikelnamn,an_article.orderid)       # One kassa can only make similar orderids                         
                    if not any(aio.accompanied_articles):                            
                        print('An order for {0:d}  with cashier id {1:d} is not qualified Rabbat item with same type.' .format(an_article.orderid,an_article.kassaid))
                    else:
                        print('Adding items for {0:d}  and cashier {1:d}' .format(an_article.orderid,an_article.kassaid))
                        popular_articles_accompanied_list[an_article.artikelnamn].append(aio)

        return popular_articles_accompanied_list
    
    @classmethod
    def extract_all_cashiers_ids(self,records):
        cashier_ids=OrderedDict()
        for current_article in records:
            if current_article.kassaid in cashier_ids:                
                cashier_ids[current_article.kassaid] += 1
            else:
                cashier_ids[current_article.kassaid] =1
        return cashier_ids
    
    @classmethod
    def create_list_of_cashier_groups_most_sold_items(self,records,cashier_groups,number_of_top_articles):
        d=OrderedDict()
        for a_cashier_group_no in cashier_groups:             
            if a_cashier_group_no != 0: # Means they are ungrouped cashiers [We do not consider them]
                group_cashiers_list=cashier_groups[a_cashier_group_no]
                popular_articles_for_cashier_group=self.__get_most_popular_articles_names(records,group_cashiers_list,number_of_top_articles)
                d[a_cashier_group_no]={'group_popular_articles':popular_articles_for_cashier_group,'included_cashiers_ids':group_cashiers_list}
        return d


    @classmethod
    def populate_accompanied_articles_for_popular_items_in_cashregisters(self,records,all_cashiers_categories_popular_articles):
        d=OrderedDict()
        '''
        for a_cashier_category,the_category_popular_articles_and_cash_registers in all_cashiers_categories_popular_articles.items():
            print("----------------- Processing a cashier group {0} -------------------".format(a_cashier_category))
            group_cash_registers_ids=the_category_popular_articles_and_cash_registers['included_cashiers_ids']
            group_popular_articles=the_category_popular_articles_and_cash_registers['group_popular_articles']
            acc_prod=OrdersProcessing.create_accompanied_products_tuples_by_names(records,group_cash_registers_ids,group_popular_articles)     # Check this method output       
            d[a_cashier_category]={'included_cashiers_ids':group_cash_registers_ids,'category_popular_articles':group_popular_articles,'accompanied_products':acc_prod}
        '''
        # Keeping orders too [.json]
        o=OrderedDict()
        for a_cashier_category,the_category_popular_articles_and_cash_registers in all_cashiers_categories_popular_articles.items():
            print("************* Processing a cashier group {0} ***************".format(a_cashier_category))
            group_cash_registers_ids=the_category_popular_articles_and_cash_registers['included_cashiers_ids']
            o[a_cashier_category]={'orders':[]}
            current_order_id=-1
            
            idx=0
            while idx<len(records):
                an_order=records[idx]
                start_idx=idx
                an_order_id_start_idx=an_order.orderid   
                articles_in_order=[]
                for n in range(start_idx,len(records)):                    
                    an_order=records[n]
                    if an_order.orderid == an_order_id_start_idx:
                        if an_order.kassaid in group_cash_registers_ids:                               
                            if an_order.antal > 0:
                                articles_in_order.append((an_order.artikelnamn,an_order.antal))
                                
                    else:
                        
                        
                        if not articles_in_order==[]:
                            print('Processing order {0} and idx:{1} for group #{2}'.format(an_order.orderid,idx,a_cashier_category))
                            o[a_cashier_category]['orders'].append({'orderid':an_order_id_start_idx,'items':articles_in_order})
                        else:
                            print('Nothing to be added for order {0}'.format(an_order.orderid))
                        idx=n
                        break
            
        return d


    def __init__(self,limit,minimum_different_articles_in_receipt):
        self.nlatest_multiatricle_orders_sql=self.__sql_cmd_nlatest_multiatricle_orders(limit,minimum_different_articles_in_receipt)


    