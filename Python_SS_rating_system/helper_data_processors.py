
from collections import OrderedDict
class CategoryProcessing:
    @classmethod
    def __sql_cmd_query_categories(self):
        cmd_str='''SELECT id,kategori,huvudkategori FROM kategori_artikel ORDER BY huvudkategori'''
        return cmd_str
    def extract_categories(self,records):
        self.main_categories=OrderedDict()
        self.sub_categories=OrderedDict()
        for ct in records:
            if ct.huvudkategori == 0:
                self.main_categories[ct.id]=ct.kategori
            else:
                is_self_to_self_cat = (self.main_categories[ct.huvudkategori].upper() == ct.kategori.upper())
                self.sub_categories[ct.id]={'parent_category':ct.huvudkategori,'parent_category_name':self.main_categories[ct.huvudkategori],'category_name':ct.kategori,'s2s_cat':is_self_to_self_cat}
    def __init__(self):
        self.query_all_categories_sql=CategoryProcessing.__sql_cmd_query_categories()  
        
# This class will process all the cashiers 
class CashierProcessing:
    @classmethod
    def __sql_cmd_query_cashiers_categories(self):
        cmd_str='''select id,rapportgrupp from kassa ORDER BY kassa.rapportgrupp'''
        return cmd_str
    @classmethod
    def extract_cashier_categories(self,cashier_records):
        d = OrderedDict()
        for cashier_id,category_id in cashier_records:
            if category_id in d:
                d[category_id].append(cashier_id)
            else:
                d[category_id]=[cashier_id]
        return d
    def __init__(self):
        self.query_all_cashiers_categories_sql=CashierProcessing.__sql_cmd_query_cashiers_categories()
    @classmethod
    def flag_listed_cashiers_in_records(self,listed_cashier_ids,cashier_groups):
        d=OrderedDict()
        for a_cashier_group_no in cashier_groups:
            list_of_cashiers=cashier_groups[a_cashier_group_no]
            for a_cashier in listed_cashier_ids:
                if a_cashier in list_of_cashiers:
                    d[a_cashier_group_no]=cashier_groups[a_cashier_group_no]    # The whole group will be rescued!
                    break
        return d