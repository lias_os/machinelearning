'''
This pakcage/library is supposed to handle and store the 
processed data and dump it properly into well-formatted file

Orders are considered as a single users
pop_item_id_as_user,a_single_acc_article_id,merged_rating_acc_item,cashier_ids_###_###_###


files formats are as follows:
'''
from orders_handling import *
import csv
class DataDumper:
    def SaveRatingsIDBased(self,group_numbers_list=[],file_path='',database_filename='db',file_name_postfix='_rating_group_',articles_as_users=True,include_categories=False):
        generated_db_filenames=OrderedDict()
        generated_group_articles_info_filenames=OrderedDict()
        generated_db_filenames_json=OrderedDict()

        if group_numbers_list == []: # List is empty 
            group_numbers_list=list(range(1,len(self.OSM.SummarizedandProcessedArticles)+1))
            print()
        #Now based on the path and data we open the file
        if file_path!='':
            file_path.replace(r'\\','/')
            file_path.replace('\\','/')
            if not file_path.endswith('/'):
                file_path +='/'
        
        full_path_files=[]
        for a_group_num in group_numbers_list:
            a_full_path=file_path+database_filename+file_name_postfix+str(a_group_num)+'.csv'
            generated_db_filenames[a_group_num]=a_full_path
            generated_group_articles_info_filenames[a_group_num]=a_full_path[:-4]+'_articles_info.csv'
            generated_db_filenames_json[a_group_num]=a_full_path[:-4]+'_ratings.json'
            full_path_files.append(a_full_path)

        for i,a_file in enumerate(full_path_files):
            with open(a_file,'w') as file:
             # first the header is written
                if articles_as_users:
                    cashiers_in_the_group=self.OSM.SummarizedandProcessedArticles[group_numbers_list[i]]['included_cashiers_ids']
                    cashiers_in_the_group_str=map(str,cashiers_in_the_group)
                    pf='-'.join(cashiers_in_the_group_str)
                    if not include_categories:
                        header_str='pop_item_id_as_user,a_single_acc_article_id,merged_rating_acc_item ==>cashier_ids_'+pf
                    else:
                        header_str='pop_item_id_as_user,a_single_acc_article_id,merged_rating_acc_item,pop_article_cat,pop_article_main_cat,acc_article_cat,acc_article_main_cat ==>cashier_ids_'+pf
                    file.write(header_str+'\n')
                    #dumping the contents
                    the_group_pop_articles=self.OSM.SummarizedandProcessedArticles[group_numbers_list[i]]['merged_normalized_rates']
                    for a_pop_article in the_group_pop_articles:
                        a_pop_article_id=self.OSM.ArticlesRef[a_pop_article]['article_id']
                        the_acc_articles=the_group_pop_articles[a_pop_article]
                        for a_acc_article,rate in the_acc_articles.items():
                            if not include_categories:
                                raw_to_write='{0},{1},{2}'.format(a_pop_article_id,self.OSM.ArticlesRef[a_acc_article]['article_id'],rate)
                            else:
                                row_to_write='{0},{1},{2},{3},{4},{5},{6}'.format(a_pop_article_id,self.OSM.ArticlesRef[a_acc_article]['article_id'],rate,\
                                    self.OSM.ArticlesRef[a_pop_article]['category'],self.OSM.ArticlesRef[a_pop_article]['main_category'],\
                                    self.OSM.ArticlesRef[a_acc_article]['category'],self.OSM.ArticlesRef[a_acc_article]['main_category'])
                            file.write(row_to_write+'\n')
                else: # Here orders are considered as users                   
                    cashiers_in_the_group=self.OSM.SummarizedandProcessedArticles[group_numbers_list[i]]['included_cashiers_ids']
                    cashiers_in_the_group_str=map(str,cashiers_in_the_group)
                    pf='-'.join(cashiers_in_the_group_str)
                    if not include_categories:
                        header_str='single_order_as_user,a_single_acc_article_id,normalized_rating_acc_item ==>cashier_ids_'+pf
                    else:
                        header_str='single_order_as_user,a_single_acc_article_id,normalized_rating_acc_item,acc_article_cat,acc_article_main_cat ==>cashier_ids_'+pf
                    file.write(header_str+'\n')
                    group_acc_prods=self.OSM.SummarizedandProcessedArticles[group_numbers_list[i]]['accompanied_products']
                    for pop_article in group_acc_prods:
                        for an_order in group_acc_prods[pop_article]:                            
                            # First storing the pop article
                            if not include_categories:
                               row_to_write='{0},{1},{2}'.format(an_order.order_id,self.OSM.ArticlesRef[an_order.name]['article_id'],0.9999)
                            else:
                               row_to_write='{0},{1},{2},{3},{4}'.format(an_order.order_id,self.OSM.ArticlesRef[an_order.name]['article_id'],0.9999,self.OSM.ArticlesRef[an_order.name]['category'],self.OSM.ArticlesRef[an_order.name]['main_category'])
                            file.write(row_to_write+'\n')
                            
                            for acc_name,rating in an_order.accompanied_articles_rates_normalized.items(): 
                                if not include_categories:
                                    row_to_write='{0},{1},{2}'.format(an_order.order_id,self.OSM.ArticlesRef[acc_name]['article_id'],round(an_order.accompanied_articles_rates_normalized[acc_name],4))
                                else:
                                    row_to_write='{0},{1},{2},{3},{4}'.format(an_order.order_id,self.OSM.ArticlesRef[acc_name]['article_id'],round(an_order.accompanied_articles_rates_normalized[acc_name],4),self.OSM.ArticlesRef[acc_name]['category'],self.OSM.ArticlesRef[acc_name]['main_category'])
                                file.write(row_to_write+'\n')
        return  (generated_db_filenames,generated_group_articles_info_filenames,generated_db_filenames_json)
    
    def SaveArticlesInRatingsFileInfo(self,_generated_db_filenames,_generated_group_articles_info_filenames): # This function should be called after SaveRatingsIDBased
        for group_num,a_db_file in _generated_db_filenames.items():
            included_article_ids=set()
            with open(a_db_file, 'r', newline='') as db_csvfile:
                rating_reader=csv.reader(db_csvfile,delimiter=',')
                next(rating_reader,None)    #Skips the header                
                for row in rating_reader:
                    included_article_ids.add(int(row[1]))
            included_article_ids_sorted=sorted(included_article_ids)

            with open(_generated_group_articles_info_filenames[group_num], 'w', newline='') as info_csvfile:
                info_csvfile.write('article id~article name~article category~article main category\n')
                for an_article_id in included_article_ids_sorted:
                    an_article=self.OSM.ArticlesRefRev[an_article_id]
                    row_to_write='{0}~{1}~{2}~{3}'.format(self.OSM.ArticlesRef[an_article]['article_id'],an_article,self.OSM.ArticlesRef[an_article]['category'],self.OSM.ArticlesRef[an_article]['main_category'])
                    info_csvfile.write(row_to_write+'\n')
                     

    def SaveGroupOrdersAsJsonFile(self,_generated_db_filenames,_generated_db_filenames_json):
          for group_num,a_db_file in _generated_db_filenames.items():
            ordersD=OrderedDict()

            with open(a_db_file, 'r', newline='') as db_csvfile:
                rating_reader=csv.reader(db_csvfile,delimiter=',')
                next(rating_reader,None)    #Skips the header                
                for row in rating_reader:
                    order_id,article_id,rate=int(row[0]),int(row[1]),float(row[2])
                    article_name=self.OSM.ArticlesRefRev[article_id]
                    if not order_id in ordersD.keys():                        
                        ordersD[order_id]=[(article_name,rate)]
                    else:
                        ordersD[order_id].append((article_name,rate))

          print()    
                    
                    
                    
      

    def SaveArticlesInfo(self,group_numbers_list=[],file_path='',database_filename='db',file_name_postfix='_articles',include_categories=True):
        if file_path!='':
            file_path.replace(r'\\','/')
            file_path.replace('\\','/')
            if not file_path.endswith('/'):
                file_path +='/'
        a_file=file_path+database_filename+file_name_postfix+'.csv'
        with open(a_file,'w') as file:
            file.write('article id~article name~article category~article main category\n')            
            for an_article in self.OSM.ArticlesRef:
                row_to_write='{0}~{1}~{2}~{3}'.format(self.OSM.ArticlesRef[an_article]['article_id'],an_article,self.OSM.ArticlesRef[an_article]['category'],self.OSM.ArticlesRef[an_article]['main_category'])
                file.write(row_to_write+'\n')
            
    # handle open exceptions
    def __init__(self, order_summary):
        if not isinstance(order_summary,OrderSummary):
            try:
                raise TypeError('Error:Your input is not of type OrderSummary')
            except TypeError:
                print('An Type exception is passed here')
                raise
        self.OSM=order_summary

            
   