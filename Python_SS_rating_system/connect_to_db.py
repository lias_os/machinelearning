'''
Everything related to database connection should get handled here
'''
import sqlalchemy
import sys
from sqlalchemy import create_engine
from sqlalchemy import text
from sqlalchemy.orm import sessionmaker,scoped_session
from collections import namedtuple

class Database:
    def __connect_db(self):
        try:
            self.engine = create_engine(self.connection_url)           
        except:
            print('Unexpected error:', sys.exc_info()[0])       
            raise
    def execute_sql(self,sql_cmd):
        try:
            self.engine
            #sql=text(sql_cmd)
            #result= self.engine.execute(sql)                        
            s = self.session()
            result=s.execute(sql_cmd)
            return result
        except:
            print('SQL execution has failed')
            raise
    def return_query_records(self,query_result):
        Record = namedtuple('Record', query_result.keys())
        records = [Record(*r) for r in query_result.fetchall()]
        return  records
           
    def __init__(self,sqlqlchemy_connection_url):
        self.connection_url = sqlqlchemy_connection_url
        self.__connect_db()
        self.session=scoped_session(sessionmaker(bind=self.engine))





